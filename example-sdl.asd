(asdf:defsystem :example-sdl
    :serial t
    :components((:file "package")
                (:file "window"))
    :depends-on(:asdf :sdl2 :sdl2-image))
