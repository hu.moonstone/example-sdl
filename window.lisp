(in-package :window)

(require 'sdl2)
(require ':sdl2-image)

(defparameter *screen-width* 640)
(defparameter *screen-height* 480)

;; メイン関数
(defun main (&key (delay-time 10000))
  (sdl2:with-init (:video)
    (sdl2:with-window (window :title "SDL2 Sample" :w *screen-width* :h *screen-height*)
      (let ((screen-surface (sdl2:get-window-surface window)))
        (sdl2:fill-rect screen-surface
                        nil
                        (sdl2:map-rgb (sdl2:surface-format screen-surface) 100 250 200))
        (sdl2:update-window window)
        (sdl2:delay delay-time)))))
